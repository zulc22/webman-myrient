var ftd = document.getElementById("filelist").getElementsByTagName("td");
var letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
var lastusedletter = -1;
for (i=0; i<ftd.length; i++) {
    var ftdi = ftd[i];
    if (ftdi.className !== "link") continue;
    var firstchar = ftdi.innerText.charAt(0);
    var letter = letters.indexOf(firstchar.toUpperCase());
    if (letter === -1) {
        letter = 0;
    }
    if (letter <= lastusedletter) continue;
    lastusedletter = letter;
    ftdi.id = "letter"+letters.charAt(letter);
}