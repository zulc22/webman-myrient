use std::{fmt, collections::HashMap};

use scraper::Selector;
use std::sync::{Arc, Mutex};
use chrono::{DateTime, offset::Utc, Duration};

type AnyError = Box<dyn std::error::Error>;

async fn get(url: &str) -> Result<String, AnyError> {
    Ok(reqwest::get(url).await?.error_for_status()?.text().await?)
}

// pub struct FileListing {
//     path: String, filename: String, filesize: String
// }
type FileListing = (String,String,String);

#[derive(Debug)]
struct MyrError {
    text: &'static str
}
impl fmt::Display for MyrError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.text)
    }
}
impl std::error::Error for MyrError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        None
    }
}

lazy_static! {
    static ref LS_CACHE: Arc<Mutex<HashMap<String, (Arc<Vec<FileListing>>, DateTime<Utc>)>>> = Arc::new(Mutex::new(HashMap::new()));
}

fn ls_cache_get(key: String) -> Option<Arc<Vec<FileListing>>> {
    let ls_cache = LS_CACHE.lock().unwrap();
    match ls_cache.get(&key) {
        Some(x) => {
            if x.1 + Duration::days(1) < Utc::now() {
                return None
            }
            Some(x.0.clone())
        },
        None => None
    }
}

fn ls_cache_set(key: String, value: Vec<FileListing>) {
    let mut ls_cache = LS_CACHE.lock().unwrap();
    ls_cache.insert(key, (Arc::new(value.clone()), Utc::now()));
    drop(ls_cache);
}

pub async fn ls(path: String) -> Result<Arc<Vec<FileListing>>, AnyError> {
    match ls_cache_get(path.clone()) {
        Some(x) => {
            println!("<MYR:HTTP> Cache return {path}");
            return Ok(x)
        },
        None => {}
    }
    println!("<MYR:HTTP> Listing {path}");
    let listing_body = get(&format!("https://myrient.erista.me/files/{path}").to_string()).await?;
    println!("<MYR:HTTP> Parsing {path}");
    let listing_html = scraper::Html::parse_document(&listing_body);
    let mut files: Vec<FileListing> = Vec::new();
    for table_row in listing_html.select(&Selector::parse("table#list tbody tr")?) {
        let mut table_row = table_row.children();
        let link = match table_row.next() {
            Some(s) => match scraper::ElementRef::wrap(s) {
                Some(s) => s,
                None => return Err(Box::new(MyrError{text:"can't wrap el 1"}))
            },
            _ => return Err(Box::new(MyrError{text:"can't get el 1"}))
        };
        let size = match table_row.next() {
            Some(s) => match scraper::ElementRef::wrap(s) {
                Some(s) => s,
                None => return Err(Box::new(MyrError{text:"can't get el 2"}))
            },
            _ => return Err(Box::new(MyrError{text:"cant wrap el 2"}))
        };
        let href = match scraper::ElementRef::wrap(link.first_child().unwrap()) {
            Some(x) => x,
            None => return Err(Box::new(MyrError{text:"cant get link href"}))
        }.attr("href").unwrap().to_string();
        let filename = if href == "../" {"..".to_string()} else {
            match scraper::ElementRef::wrap(link.first_child().unwrap()) {
                Some(x) => x,
                None => return Err(Box::new(MyrError{text:"cant get link href"}))
            }.attr("title").unwrap().to_string()
        };
        let filesize = match size.text().next() {
            Some(x) => x,
            None => return Err(Box::new(MyrError{text:"cant get size text"}))
        }.to_string();
        let filesize = if filesize == "-" {
            "dir".to_string()
        } else {
            filesize
        };

        files.push((href,filename,filesize));
    }
    println!("<MYR:HTTP> Listed {path}, {} entries", files.len());
    ls_cache_set(path, files.clone());

    Ok(Arc::new(files))
}