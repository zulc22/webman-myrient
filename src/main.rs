#[macro_use] extern crate rocket;
#[macro_use] extern crate lazy_static;

mod mclient_http;
mod headerguards;

use std::ffi::OsStr;
use std::fs;
use std::path::PathBuf;

use rocket::fs::FileServer;
use rocket::http::Status;
use rocket::request::{self, Outcome, Request, FromRequest};
use rocket::response::Redirect;
use rocket_dyn_templates::Template;
use rocket_dyn_templates::context;
use rocket::http::RawStr;
use rocket::*;

#[get("/")]
fn index(host: headerguards::Host) -> Redirect {
    // http://{host} bypasses PS3 browser bug (redirects to /... strip the port number)
    Redirect::to(format!("http://{host}/files/"))
}

#[get("/files")]
async fn filelist_root() -> Template {
    filelist(PathBuf::from("")).await
}

#[get("/files/<path..>")]
async fn filelist(path: PathBuf) -> Template {
    let path = path.to_str().unwrap().to_string();
    let l = mclient_http::ls(path.clone()).await.expect("msg");
    Template::render("filelist", context! {list: l.as_ref(), path: path})
}

#[get("/dd/<filename>")]
fn downloaddirect(referer: headerguards::Referer, host: headerguards::Host, filename: &str) -> Redirect {
    let mut filepath = PathBuf::from(referer.0);
    filepath.push(filename);
    let mut fpc: Vec<_> = filepath.iter().collect();
    fpc.remove(0); fpc.remove(0); fpc.remove(0);
    let filepath: PathBuf = fpc.iter().collect();
    let binding = filepath.to_str().unwrap();
    let filepath = RawStr::new(&binding).percent_decode().unwrap();
    let filepath = filepath.to_string();
    
    let mut guess = "rom";

    if filepath.contains("Sony - PlayStation 3") {
        guess = "ps3";
    } else if filepath.contains("Sony - PlayStation 2") {
        guess = "ps2";
    } else if filepath.contains("Sony - PlayStation") {
        guess = "psx";
    } else if filepath.contains("BD-Video") {
        guess = "bluray";
    } else if filepath.contains("DVD-Video") {
        guess = "dvd";
    }

    #[cfg(windows)] {
        let filepath = filepath.replace(r"\", "/");
    }
    let filepath = RawStr::new(&filepath).percent_encode().to_string();

    // http://{host} bypasses PS3 browser bug (redirects to /... strip the port number)
    Redirect::to(format!("http://{host}/confirm/{guess}/{filepath}"))
}

#[get("/confirm/<contenttype>/<filepath>")]
fn downloadconfirm(contenttype: &str, filepath: &str) -> Template {
    Template::render("downloadconfirm", context!{
        fp: filepath,
        guess: contenttype
    })
}

#[post("/download")]
fn startdownload() {

}

#[catch(404)]
fn four04() -> String {
    String::from_utf8(fs::read("templates/404.html").unwrap()).unwrap()
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .attach(Template::fairing())
        .register("/", catchers![four04])
        .mount("/", routes![
            index,
            filelist_root,
            filelist,
            downloaddirect,
            downloadconfirm
        ])
        .mount("/static", FileServer::from("static"))
}