use std::{ops::Deref, fmt};

use rocket::{request::{FromRequest, Outcome}, Request, http::Status};

pub struct Referer<'r>(pub &'r str);
#[rocket::async_trait]
impl<'r> FromRequest<'r> for Referer<'r> {
    type Error = ();

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        match req.headers().get_one("Referer") {
            None => Outcome::Error((Status::BadRequest, ())),
            Some(key) => Outcome::Success(Self(key))
        }
    }
}
impl<'r> Deref for Referer<'r> {
    type Target = &'r str;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'r> fmt::Display for Referer<'r> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}


pub struct Host<'r>(pub &'r str);
#[rocket::async_trait]
impl<'r> FromRequest<'r> for Host<'r> {
    type Error = ();

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        match req.headers().get_one("Host") {
            None => Outcome::Error((Status::BadRequest, ())),
            Some(key) => Outcome::Success(Self(key))
        }
    }
}
impl<'r> Deref for Host<'r> {
    type Target = &'r str;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'r> fmt::Display for Host<'r> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}