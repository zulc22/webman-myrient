# webman-myrient

Web software to download and install PS3 games from Myrient.

Very work in progress; requirements aren't nessecarily checked, yet required, or could change.

Web-facing components are designed to be compatible with Safari 5.1 (for Windows), a relatively close analog to the PS3's native browser, given the age and WebKit engine.

Requires:

- Modern PC
- - Rust (programming language)
- - Reasonable (~20G?) disk space left after compilation, to not run out of space while decoding.
- - ps3dec (for decoding and installing PS3 games).
- - - [Windows build at romhacking.net](1)
- - - [Source code on GitHub](2)
- - - [AUR package for Arch users (`ps3dec-git`)](3)
- Modified PlayStation 3
- - CFW with WebMAN
- - Wi-Fi or Ethernet connection to Modern PC

[1]: https://www.romhacking.net/utilities/1456
[2]: https://github.com/al3xtjames/PS3Dec
[3]: https://aur.archlinux.org/packages/ps3dec-git